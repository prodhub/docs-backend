#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CURRENT_DIR="$(pwd)"

echo "Copy docs.."

mkdir -p $DIR/../docs/$2

echo $DIR/../docs/$2/

cp -r $1/* $DIR/../docs/$2

echo "Regenerate docs.."

cd $DIR/../daux/ && php generate --source="$DIR/../docs/" --destination="$DIR/../static/"
cd $CURRENT_DIR
